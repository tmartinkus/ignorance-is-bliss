import fns from './functions';

export default {
  isIpadOS() {
    return (/MacIntel/.test(navigator.platform) || /iPad/i.test(navigator.userAgent) || /iPhone OS/i.test(navigator.userAgent)) && window.innerWidth >= 900;
  },
  init() {
    const self = this;

    if (localStorage.getItem('scrollType') === null) {
      self.setScrollDetectionEvent();
    }

    // $('.research--column').each(function() {
    //   const width = parseFloat($(this).attr('data-width')) / 100;
    //   $(this).css('min-width', 1200 * width);
    // });

    $('.research--head').each(function () {
      const head = $(this);
      const research = head.parent();
      const body = research.find('.research--body');

      const offset = research.offset().top;

      research.find('.burger--button').on('click', function () {
        research.find('.research--progress').toggleClass('burger-open');
      });

      if (self.isIpadOS()) {
        body.css('overflow', 'unset').css('overflow', 'scroll hidden');
      }

      head.find('.animated-heading').on('click', function () {
        const scrollType = localStorage.getItem('scrollType');
        self.unsetScrollDetectionEvent();
        body.scrollLeft(0);

        research.find('[data-src]').each(function () {
          $(this).attr('src', $(this).attr('data-src'));
        });

        window.location.hash = '#' + research.attr('data-slug');

        if (scrollType === 'trackpad') {
          body.css('overflow-x', 'scroll');
        }

        research.find('.research--progress-item').on('click', function () {
          const index = parseInt($(this).attr('data-index'));
          const offset = $($(this).attr('data-target')).offset().left + body.scrollLeft();

          if (index === 0 || offset !== 0) {
            body.animate({
              scrollLeft: offset
            }, 500);
          }
        });

        $([document.documentElement, document.body]).animate({
          scrollTop: offset
        }, 500, 'swing', () => {
          $('body').addClass('no-scroll');
          $('.header').fadeOut();

          setTimeout(function () {
            body.addClass('open');
            research.find('.research--header').fadeIn();
            $('#research-cursor-text, #next-research-cursor-text').hide();
          }, 500);
        });

        body.on('mousewheel', function (event) {
          if (!fns.isMobile() && scrollType === 'mouse') {
            event.preventDefault();
            self.trackProgress($(this));

            const delta = event.originalEvent.wheelDeltaY || -event.originalEvent.deltaY;
            $(this).scrollLeft($(this).scrollLeft() - delta * .75);

            if ($(this).scrollLeft() > $(this).parent().width() - window.innerWidth * 1.5) {
              let offset = Math.floor(
                $(this).find('.research--body-scroll-container').width() - $(this).width() - $(this).scrollLeft()
              );

              if (offset < 0) {
                offset = 0;
              }

              $(this).find('.animated-heading').children().each(function () {
                const x = parseFloat($(this).attr('data-x-multiplier')) * offset;
                const y = parseFloat($(this).attr('data-y-multiplier')) * offset;
                const rot = parseFloat($(this).attr('data-rot-multiplier')) * offset;

                $(this).css('transform', ` rotate(${rot}deg) translate(${x}%, ${y}%)`);
              });
            }
          }
        });
      });

      $('.research--back').on('click', function () {
        research.find('.research--header').fadeOut();
        $('body').removeClass('no-scroll');
        $('.header').fadeIn();
        body.removeClass('open');
        body.unbind('mousewheel');
        self.removeHash();
      });

      body.find('.animated-heading').on('click', function () {
        let nResearch = research.next();

        if (research.is(':last-child')) {
          nResearch = research.siblings().first();
        }

        research.find('.research--back').trigger('click');
        nResearch.find('.research--head .animated-heading').trigger('click');
      });

      body.on('scroll', function (e) {
        self.trackProgress($(this));
        if (/(Mac|iPhone|iPod|iPad)/i.test(navigator.platform)) {
          e.preventDefault();
        }
      });
    });

    if (window.location.hash) {
      $(`[data-slug=${window.location.hash.replace('#', '').replace('/', '')}]`).find('.research--head .animated-heading').trigger('click');
    }
  },
  trackProgress(el) {
    if (!fns.isMobile()) {
      el.parent().find('.research--progress-bar').css('width', (el.scrollLeft() / (el.get(0).scrollWidth - window.innerWidth) * 100) + '%')
      el.parent().find('.research--progress-item').removeClass('open');

      el.find('.research--section').each(function () {
        if ($(this).offset().left < 1) {
          $($(this).attr('data-target')).addClass('open');
        }
      });
    }
  },
  detectScrollType(e) {
    if (e.originalEvent.wheelDeltaX !== 0) {
      localStorage.setItem('scrollType', 'trackpad');
    } else if (localStorage.getItem('scrollType') !== 'trackpad') {
      localStorage.setItem('scrollType', 'mouse');
    }
  },
  setScrollDetectionEvent() {
    $(window).on('mousewheel', this.detectScrollType);
  },
  unsetScrollDetectionEvent() {
    $(window).off('mousewheel', this.detectScrollType);
  },
  removeHash() {
    history.pushState("", document.title, window.location.pathname
      + window.location.search);
  }
}
