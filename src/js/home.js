export default {
  init() {
    this.initNextButton()
    this.initGalleryInfo();
  },
  initNextButton() {
    $('.home--hero-next-button').on('click', function () {
      $([document.documentElement, document.body]).animate({
        scrollTop: $('.home--research-list').offset().top
      }, 1000);
    });
  },
  initGalleryInfo() {
    $('.home--gallery-slider').on('beforeChange', function () {
      $('.home--gallery-info-item').fadeOut();
    }).on('afterChange', function (event, slick, currentSlide) {
      $(`#gallery-info-${currentSlide}`).fadeIn();
    });
  }
}
