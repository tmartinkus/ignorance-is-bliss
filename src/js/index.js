import $ from 'jquery';
import 'jquery-ui/ui/effect.js';

import '../scss/style.scss';
import '../scss/helpers/_helpers.scss';
import '../scss/blocks/_blocks.scss';
import '../scss/pages/_pages.scss';

import fns from "./functions";
import general from "./general";
import newsItem from "./news-item";
import home from "./home";
import research from "./research";
import product from "./product";

import 'slick-slider';
import 'slick-slider/slick/slick.scss';
import 'slick-slider/slick/slick-theme.scss';

$(document).ready(function () {
  fns.init();
  general.init();
  home.init();
  research.init();
  newsItem.init();
  product.init();
});
