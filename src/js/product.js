const re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

export default {
  init() {
    this.initGallery();
    this.initAccordions();
    this.initGrid();
    this.initForms();

    $(window).on('resize', () => {
      this.initGrid();
    });
  },
  initGrid() {
    $('.variation-grid-item').each(function () {
      if (window.innerWidth <= 1100) {
        $(this).css('width', $(this).attr('data-mobile-width'))
      } else if (window.innerWidth <= 1400) {
        $(this).css('width', $(this).attr('data-laptop-width'))
      } else {
        $(this).css('width', $(this).attr('data-desktop-width'))
      }
    });
  },
  initGallery() {
    const gallery = $('.product--variations-gallery-slider');
    const info = $('.product--variations-gallery-info-slider');

    info.slick({
      arrows: false,
      dots: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      adaptiveHeight: true,
      focusOnSelect: false,
      asNavFor: '.product--variations-gallery-slider'
    });

    gallery.slick({
      arrows: false,
      dots: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      adaptiveHeight: true,
      asNavFor: '.product--variations-gallery-info-slider'
    });

    $('.variation-grid-item').on('click', function () {
      gallery.slick('slickGoTo', parseInt($(this).attr('data-index')));

      $([document.documentElement, document.body]).animate({
        scrollTop: gallery.offset().top - 80
      }, 500);
    });

    $('.product--variations-gallery-slider-item').on('click', function () {
      if (window.innerWidth <= 1100) {
        $(this).toggleClass('hover');
      }
    });

    $('.product--variations-gallery-right').on('click', function () {
      gallery.slick('slickNext');
    });
    $('.product--variations-gallery-left').on('click', function () {
      gallery.slick('slickPrev');
    });
  },
  initAccordions() {
    $('.product--accordion-head').on('click', function () {
      const body = $(this).parent().find('.product--accordion-body');
      $(this).parent().toggleClass('open');

      if ($(this).parent().hasClass('open')) {
        body.height(body.get(0).scrollHeight);
      } else {
        body.height(0);
      }
    });
  },
  initForms() {
    const self = this;

    $('[name=shipping]').on('change', function () {
      const form = $(this).parents('form');
      form.find('.shipping-fields').toggleClass('open');
      setTimeout(() => {
        self.adjustFormContainerHeight(form);
      }, 500);
    });

    $('[name=client-type]').on('change', function () {
      const form = $(this).parents('form');
      form.find('.business-fields').toggleClass('open');
      setTimeout(() => {
        self.adjustFormContainerHeight(form);
      }, 500);
    });

    $('form input').on('keyup focus', function () {
      $(this).removeClass('invalid');
    });

    $('.product--accordion-sample-form').on('submit', function (e) {
      e.preventDefault();

      if (self.validateSampleForm($(this))) {
        $.ajax({
          method: 'POST',
          data: {
            form: 'sample',
            ...self.buildData($(this))
          },
          url: '',
        }).then(resp => {
          resp = JSON.parse(resp);
          if (resp.status) {
            $('.product--accordion-sample-success').fadeIn(400);
            setTimeout(() => {
              self.adjustFormContainerHeight($(this))
            }, 410)
            self.clearFields($(this));
          }
        });
      }
    });

    $('.product--accordion-request-form').on('submit', function (e) {
      e.preventDefault();

      if (self.validateRequestForm($(this))) {
        $.ajax({
          method: 'POST',
          data: {
            form: 'request',
            ...self.buildData($(this))
          },
          url: '',
        }).then(resp => {
          resp = JSON.parse(resp);
          if (resp.status) {
            $('.product--accordion-request-success').fadeIn(400);
            setTimeout(() => {
              self.adjustFormContainerHeight($(this))
            }, 410)
            self.clearFields($(this));
          }
        });
      }
    });
  },
  field(form, name) {
    return form.find(`[name=${name}]`);
  },
  value(form, name) {
    return this.field(form, name).val();
  },
  isChecked(form, name) {
    return this.field(form, name).is(':checked');
  },
  clearFields(form) {
    form.find('input[type=text], textarea').val('');
  },
  clearValidation(form) {
    form.find('input[type=text], textarea').removeClass('invalid');
    form.find('.errors > *').fadeOut();
  },
  showErr(form, name, suffix = 'err') {
    form.find(`.${name}-${suffix}`).fadeIn();
    form.find(`[name=${name}]`).addClass('invalid');
  },
  adjustFormContainerHeight(form) {
    const el = form.parent().parent();
    form.parent().parent().parent().height(parseFloat(el.css('padding-top').replace('px', '')) + parseFloat(el.css('padding-top').replace('px', '')) + el.height());
  },
  validateSampleForm(form) {
    let isValid = true;

    this.clearValidation(form);

    if (form.find('[name=client-type]:checked').val() === 'business') {
      if (!this.value(form, 'company')) {
        isValid = false;
        this.showErr(form, 'company');
      }

      if (!this.value(form, 'vat')) {
        isValid = false;
        this.showErr(form, 'vat');
      }

      if (!this.isChecked(form, 'shipping')) {
        if (!this.value(form, 'shipping-address')) {
          isValid = false;
          this.showErr(form, 'shipping-address');
        }

        if (!this.value(form, 'shipping-city')) {
          isValid = false;
          this.showErr(form, 'shipping-city');
        }

        if (!this.value(form, 'shipping-zip')) {
          isValid = false;
          this.showErr(form, 'shipping-zip');
        }

        if (!this.value(form, 'shipping-country')) {
          isValid = false;
          this.showErr(form, 'shipping-country');
        }
      }
    }

    if (!this.value(form, 'name')) {
      isValid = false;
      this.showErr(form, 'name');
    }

    if (!this.value(form, 'email')) {
      isValid = false;
      this.showErr(form, 'email');
    } else if (!re.test(this.value(form, 'email'))) {
      isValid = false;
      this.showErr(form, 'email', 'invalid');
    }

    if (!this.value(form, 'phone')) {
      isValid = false;
      this.showErr(form, 'phone');
    }

    if (!this.value(form, 'address')) {
      isValid = false;
      this.showErr(form, 'address');
    }

    if (!this.value(form, 'city')) {
      isValid = false;
      this.showErr(form, 'city');
    }

    if (!this.value(form, 'zip')) {
      isValid = false;
      this.showErr(form, 'zip');
    }

    if (!this.value(form, 'country')) {
      isValid = false;
      this.showErr(form, 'country');
    }

    this.adjustFormContainerHeight(form);

    return isValid;
  },
  validateRequestForm(form) {
    let isValid = true;

    this.clearValidation(form);

    if (!this.value(form, 'name')) {
      isValid = false;
      this.showErr(form, 'name');
    }

    if (!this.value(form, 'email')) {
      isValid = false;
      this.showErr(form, 'email');
    } else if (!re.test(this.value(form, 'email'))) {
      isValid = false;
      this.showErr(form, 'email', 'invalid');
    }

    if (!this.value(form, 'country')) {
      isValid = false;
      this.showErr(form, 'country');
    }

    if (!this.value(form, 'message')) {
      isValid = false;
      this.showErr(form, 'message');
    }

    this.adjustFormContainerHeight(form);

    return isValid;
  },
  buildData(form) {
    let data = {};

    form.find('input, textarea').each(function () {
      if ($(this).attr('name') === 'client-type') {
        data['client-type'] = form.find('[name=client-type]:checked').val();
      } else {
        data[$(this).attr('name')] = $(this).val();
      }
    });

    return data;
  }
}
