import fns from './functions';

export default {
  init() {
    this.initBurger();
    this.initGalleries();
    this.animateHeadings();
    this.animateHeading();
    this.initCursorText();
    this.animateIcons();
    this.initBackToTop();
    this.initCookies();
  },
  initCookies(){
    if (!localStorage.getItem('popup_shown')) {
      $('#cookies').fadeIn();
    }

    $('.cookies--close').on('click', function() {
      $('#cookies').fadeOut();
      localStorage.setItem('popup_shown', 'true'); // Set the flag in localStorage
    });
  },
  animateIcons() {
    $('.animated-icon').each(function () {
      $(this).on('mouseenter', function () {
        if (!fns.isMobile()) {
          $(this).children().each(function () {
            if (!$(this).hasClass('static')) {
              const x = Math.random() * 75 * (Math.random() > 0.5 ? -1 : 1);
              const y = Math.random() * 7 * (Math.random() > 0.5 ? -1 : 1);
              const rot = Math.random() * 90 * (Math.random() > 0.5 ? -1 : 1);

              $(this).css('transform', `translate(${x}%, ${y}%) rotate(${rot}deg)`)
            }
          });
        }
      }).on('mouseleave', function () {
        $(this).children().each(function () {
          if (!$(this).hasClass('static')) {
            $(this).css('transform', 'translate(0, 0)')
          }
        });
      });
    });
  },
  initCursorText() {
    const researchCursor = $('#research-cursor-text');
    const nextResearchCursor = $('#next-research-cursor-text');

    $('.research--head .animated-heading').on('mouseenter', function (e) {
      researchCursor.css('top', e.clientY).css('left', e.clientX - 50).show();
    }).on('mouseleave', function () {
      researchCursor.hide();
    }).on('mousemove', function (e) {
      if (!researchCursor.is(':visible')) {
        researchCursor.show();
      }

      researchCursor.css('top', e.clientY).css('left', e.clientX - 50);
    });

    $('.research--body .animated-heading').on('mouseenter', function (e) {
      nextResearchCursor.css('top', e.clientY).css('left', e.clientX - 50).show();
    }).on('mouseleave', function () {
      nextResearchCursor.hide();
    }).on('mousemove', function (e) {
      if (!nextResearchCursor.is(':visible')) {
        nextResearchCursor.show();
      }

      nextResearchCursor.css('top', e.clientY).css('left', e.clientX - 50);
    });
  },
  initBurger() {
    $('.burger--button').on('click', function () {
      $(this).toggleClass('open');
      $('.burger').toggleClass('open');
    });

    if ($('.home').get(0)) {
      $(window).on('scroll', function () {
        if (window.innerWidth <= 1100) {
          const header = $('header.header');
          if ($('.home--news').offset().top - $(window).scrollTop() < 1) {
            header.removeClass('header---homepage');
          } else {
            header.addClass('header---homepage');
          }
        }
      });
    }
  },
  initGalleries() {
    $('.gallery').each(function () {
      const dots = $(this).attr('data-dots') === 'true';
      const adaptiveHeight = $(this).attr('data-adaptive-height') === 'true';
      const el = $(this);

      el.slick({
        arrows: false,
        dots,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        adaptiveHeight
      });

      const parent =  $(this).parents('.product--accordion');
      if (parent) {
        el.on('beforeChange', function (e, slick, currSlide, nextSlide) {
          parent.find('.product--accordion-gallery-info-item').fadeOut();

          setTimeout(function () {
            parent.find(`.gallery-info-${nextSlide}`).fadeIn();
          }, 500);
        })
      }

      el.parent().find('.gallery--right').on('click', function () {
        el.slick('slickNext');
      });
      el.parent().find('.gallery--left').on('click', function () {
        el.slick('slickPrev');
      });
    })
  },
  animateHeadings() {
    const self = this;
    const translateRange = 20;
    const rotateRange = 60;

    $('.animated-heading').children().each(function () {
      $(this).attr('data-x-multiplier', Math.random() * translateRange * (Math.random() > 0.5 ? 1 : -1) / window.innerHeight)
        .attr('data-y-multiplier', Math.random() * translateRange * (Math.random() > 0.5 ? 1 : -1) / window.innerHeight)
        .attr('data-rot-multiplier', Math.random() * rotateRange * (Math.random() > 0.5 ? 1 : -1) / window.innerHeight);
    });

    $(window).on('scroll', function () {
      self.animateHeading();
    });
  },
  animateHeading() {
    $('.home--hero').each(function () {
      let offset = $(this).offset().top - $(window).scrollTop();

      if (offset > 0) {
        offset = 0;
      }

      if (Math.abs(offset) < window.innerHeight * 1.5) {
        $(this).find('.animated-heading:not(.no-animation)').children().each(function () {
          const x = parseFloat($(this).attr('data-x-multiplier')) * offset;
          const y = parseFloat($(this).attr('data-y-multiplier')) * offset;
          const rot = parseFloat($(this).attr('data-rot-multiplier')) * offset;

          $(this).css('transform', ` rotate(${rot}deg) translate(${x}%, ${y}%)`)
        });
      }
    })

    $('.research').each(function () {
      let offset = $(this).offset().top - $(window).scrollTop();

      if (offset < 0) {
        offset = 0;
      }

      if (Math.abs(offset) < window.innerHeight * 1.5) {
        $(this).find('.animated-heading:not(.no-animation)').children().each(function () {
          const x = parseFloat($(this).attr('data-x-multiplier')) * offset;
          const y = parseFloat($(this).attr('data-y-multiplier')) * offset;
          const rot = parseFloat($(this).attr('data-rot-multiplier')) * offset;

          $(this).css('transform', ` rotate(${rot}deg) translate(${x}%, ${y}%)`)
        });
      }
    });
  },
  initBackToTop() {
    $('#back-to-top').on('click', function () {
      $([document.documentElement, document.body]).animate({
        scrollTop: 0
      }, 1000);
    })
  }
}
