export default {
  init() {
    this.initArticles();
  },
  initArticles() {
    $('.news-list-item--head').on('click', function () {
      const body = $(this).parent().find('.news-list-item--body');
      $(this).parent().toggleClass('open');

      if ($(this).parent().hasClass('open')) {
        body.height(body.get(0).scrollHeight);
      } else {
        body.height(0);
      }
    })
  }
}
