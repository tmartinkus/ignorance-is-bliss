<?php

return [
    'bodyWidth' => function ($page) {
        $width = 0;

        foreach (page()->children()->listed() as $section) {
            $width += intval($section->width());
        }

        return $width;
    }
];