<?php snippet('header') ?>

<section class="news-list">
  <div class="container">
    <?php foreach (page()->children()->listed() as $item): ?>
      <?php snippet('news-list-item', ['item' => $item]) ?>
    <?php endforeach; ?>
  </div>
</section>

<?php snippet('footer') ?>
