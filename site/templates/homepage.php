<?php snippet('header') ?>

<?php //die(phpinfo()); ?>

  <section class="home--hero">
    <?php snippet('headings/home') ?>

    <h1 class="home--hero-subheading">
      <?= page()->subheading() ?>
    </h1>

    <?php snippet('icons/down', ['class' => 'home--hero-next-button']) ?>
  </section>

  <section class="home--research-list">
    <?php $i = 0;
    $headings = [];
    foreach (page()->children()->listed() as $item) {
      $headings[] = $item->heading();
    } ?>

    <?php $i = 1; ?>
    <?php foreach (page()->children()->listed() as $item): ?>
      <?php if ($i === sizeof(page()->children()->listed()))
        $i = 0;
      ?>
      <?php snippet('research', ['item' => $item, 'end' => $headings[$i]]) ?>
      <?php $i++; ?>
    <?php endforeach; ?>
  </section>

  <section class="home--news">
    <div class="container">
      <h2 class="home--heading">
        News
      </h2>

      <?php foreach ($pages->find('news')->children()->limit(3) as $item): ?>
        <?php snippet('news-list-item', ['item' => $item]); ?>
      <?php endforeach; ?>
    </div>
  </section>


  <section class="home--gallery">
    <div class="container">
      <h2 class="home--heading">
        Gallery
      </h2>

      <div class="home--gallery-container">
        <div class="home--gallery-info">
          <?php foreach (page()->gallery()->toStructure() as $i => $item): ?>
            <div class="home--gallery-info-item" id="gallery-info-<?= $i ?>"
                 style="<?= $i !== 0 ? 'display: none;' : '' ?>">
              <?= $item->title()->kirbyText() ?>
            </div>
          <?php endforeach; ?>
        </div>

        <div class="gallery--container">
          <div class="gallery home--gallery-slider" data-dots="true">
            <?php foreach (page()->gallery()->toStructure() as $item): ?>
              <div class="gallery--item">
                <?= $item->image()->toFile()->resize(2400)->html(); ?>
              </div>
            <?php endforeach; ?>
          </div>

          <?php if (sizeof(page()->gallery()->toStructure()) > 1): ?>
            <div class="gallery--left"></div>
            <div class="gallery--right"></div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </section>

  <div id="research-cursor-text">Research</div>
  <div id="next-research-cursor-text">Next research</div>
<?php snippet('footer') ?>