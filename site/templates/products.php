<?php snippet('header') ?>

<section class="products">
  <div class="container">
    <div class="products--hero-text">
      <?= page()->hero()->kirbyText() ?>
    </div>

    <div class="products--list">
      <?php foreach (page()->children()->listed() as $product): ?>
        <?php snippet('product-list-item', ['product' => $product]) ?>
      <?php endforeach; ?>
    </div>
  </div>
</section>

<?php snippet('footer') ?>
