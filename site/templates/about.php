<?php snippet('header') ?>

  <section class="about">
    <div class="container">
      <div class="about--hero">
        <div class="about--hero-text">
          <?= page()->hero()->kirbyText() ?>
        </div>

        <div class="about--hero-contacts">
          <h2 class="about--hero-contacts-heading">Contact</h2>

          <div class="about--hero-contacts-item">
            <p class="about--hero-contacts-item-field">Email</p>
            <div class="about--hero-contacts-item-value"><?= page()->email()->kirbyText() ?></div>
          </div>
          <div class="about--hero-contacts-item">
            <p class="about--hero-contacts-item-field">Phone</p>
            <a href="tel:<?= page()->phone() ?>" class="about--hero-contacts-item-value"><?= page()->phone() ?></a>
          </div>
          <div class="about--hero-contacts-item">
            <p class="about--hero-contacts-item-field">Instagram</p>
            <a target="_blank" href="<?= page()->instagramUrl() ?>"
               class="about--hero-contacts-item-value"><?= page()->instagramUsername() ?></a>
          </div>

          <div class="about--hero-contacts-item">
            <div class="about--hero-contacts-item-value">
              <?= page()->extraInfo()->kirbyText() ?>
            </div>
          </div>
        </div>
      </div>

      <div class="about--gallery">
        <div class="about--gallery-text">
          <?= page()->galleryText()->kirbyText() ?>
        </div>

        <div class="about--gallery-container">
          <?php snippet('gallery', ['items' => page()->gallery()->toFiles(), 'dots' => true, 'width' => 1215, 'ratio' => page()->galleryRatio()->html()]) ?>
        </div>
      </div>

      <div class="about--awards">
        <h2 class="about--awards-heading">
          Awards
        </h2>


        <div class="about--awards-list">
          <?php foreach (page()->awards()->toStructure() as $award): ?>
            <a href="<?= $award->url() ?>" target="_blank" class="about--awards-list-item">
              <?= $award->image()->toFile()->html() ?>
              <!--              <div class="about--awards-list-item-image"
                   style="background-image: url('<? /*= $award->image()->toFile()->url() */ ?>')"></div>-->
              <span class="about--awards-list-item-title">
                <?= $award->title() ?>
              </span>
            </a>
          <?php endforeach; ?>
        </div>
      </div>

      <div class="about--partnership">
        <h2 class="about--partnership-heading">
          Partnerships and collaborations
        </h2>

        <div class="about--partnership-columns">
          <div class="about--partnership-column">
            <h3 class="about--partnership-column-heading">
              Material Libraries:
            </h3>

            <?php foreach (page()->materialLibraries()->toStructure() as $link): ?>
              <a href="<?= $link->url() ?>" target="_blank" class="about--partnership-column-item">
                <?= $link->text() ?>
              </a>
            <?php endforeach; ?>
          </div>

          <div class="about--partnership-column">
            <h3 class="about--partnership-column-heading">
              Waste Supply:
            </h3>

            <?php foreach (page()->wasteSupply()->toStructure() as $link): ?>
              <a href="<?= $link->url() ?>" target="_blank" class="about--partnership-column-item">
                <?= $link->text() ?>
              </a>
            <?php endforeach; ?>
          </div>

          <div class="about--partnership-column">
            <h3 class="about--partnership-column-heading">
              Manufacturers:
            </h3>

            <?php foreach (page()->manufacturers()->toStructure() as $link): ?>
              <a href="<?= $link->url() ?>" target="_blank" class="about--partnership-column-item">
                <?= $link->text() ?>
              </a>
            <?php endforeach; ?>
          </div>

          <?php if (page()->showCustomPartnership()->toBool()): ?>
            <div class="about--partnership-column">
              <h3 class="about--partnership-column-heading">
                <?= page()->customPartnershipHeading()->toString() ?>:
              </h3>

              <?php foreach (page()->customPartnership()->toStructure() as $link): ?>
                <a href="<?= $link->url() ?>" target="_blank" class="about--partnership-column-item">
                  <?= $link->text() ?>
                </a>
              <?php endforeach; ?>
            </div>
          <?php endif; ?>
        </div>
      </div>

    </div>
  </section>

<?php snippet('footer') ?>