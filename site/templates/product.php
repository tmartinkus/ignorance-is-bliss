<?php
if (kirby()->request()->is('POST')) {
  $data = $_POST;
  $email = [
    'from' => 'info@ignorance-bliss.com',
    'to' => site()->adminEmail()->value,
  ];

  if ($data['form'] === 'sample') {
    $email['subject'] = 'New IIB sample box form submission!';
    $shipping = $data['shipping'] === 'on';
    $email['body'] = 'Product: ' . page()->title() . PHP_EOL .
      'Client type: ' . $data['client-type'] . PHP_EOL .
      'Company name: ' . $data['company'] . PHP_EOL .
      'VAT code: ' . $data['vat'] . PHP_EOL .
      'Name: ' . $data['name'] . PHP_EOL .
      'Email address: ' . $data['email'] . PHP_EOL .
      'Phone number: ' . $data['phone'] . PHP_EOL .
      'Billing address: ' . $data['address'] . PHP_EOL .
      'Billing city: ' . $data['city'] . PHP_EOL .
      'Billing postal code: ' . $data['zip'] . PHP_EOL .
      'Billing country: ' . $data['country'] . PHP_EOL .
      'Shipping address: ' . ($shipping ? $data['address'] : $data['shipping-address']) . PHP_EOL .
      'Shipping city: ' . ($shipping ? $data['city'] : $data['shipping-city']) . PHP_EOL .
      'Shipping postal code: ' . ($shipping ? $data['zip'] : $data['shipping-zip']) . PHP_EOL .
      'Shipping country: ' . ($shipping ? $data['country'] : $data['shipping-country']) . PHP_EOL;
  } else {
    $email['subject'] = 'New IIB request form submission!';
    $email['body'] = 'Product: ' . page()->title() . PHP_EOL .
      'Name: ' . $data['name'] . PHP_EOL .
      'Company name: ' . $data['company'] . PHP_EOL .
      'Email address: ' . $data['email'] . PHP_EOL .
      'Country: ' . $data['country'] . PHP_EOL .
      'Message: ' . $data['message'] . PHP_EOL;
  }

  $status = false;

  try {
    $status = kirby()->email($email)->isSent();
  } catch (Exception $e) {
    echo json_encode([
      'status' => $status,
      'error' => $e->getMessage(),
      'email' => $email
    ]);
    die();
  }

  echo json_encode([
    'status' => $status
  ]);
  die();
}
?>

<?php snippet('header') ?>

  <div class="product">
    <div class="container">

      <h2 class="product--hero-text">
        <?= page()->heroText()->kirbyText() ?>
      </h2>

      <div class="product--variations">
        <h3 class="product--variations-heading desktop-only">
          <?= page()->variationListLabel()->html() ?>
        </h3>

        <div class="product--variations-list---mobile product--accordion mobile-only">
          <div class="product--accordion-head">
            <h3 class="product--accordion-head-title">
              <?= page()->variationListLabel()->html() ?>
            </h3>

            <?php snippet('icons/expand', ['class' => 'product--accordion-head-icon']) ?>

          </div>

          <div class="product--accordion-body">
            <div class="product--accordion-body-inner">
              <?php $index = 0; ?>
              <?php foreach (page()->children()->listed() as $variation): ?>

                <?php snippet('variation-grid-item', [
                  'index' => $index,
                  'variation' => $variation,
                  'desktopColumns' => page()->desktopColumns(),
                  'laptopColumns' => page()->laptopColumns(),
                  'mobileColumns' => page()->mobileColumns()
                ]) ?>

                <?php $index++; ?>

              <?php endforeach; ?>
            </div>
          </div>
        </div>

        <div class="product--variations-list desktop-only">
          <?php $index = 0; ?>
          <?php foreach (page()->children()->listed() as $variation): ?>

            <?php snippet('variation-grid-item', [
              'index' => $index,
              'variation' => $variation,
              'desktopColumns' => page()->desktopColumns(),
              'laptopColumns' => page()->laptopColumns(),
              'mobileColumns' => page()->mobileColumns()
            ]) ?>

            <?php $index++; ?>

          <?php endforeach; ?>
        </div>

        <div class="product--variations-gallery">
          <div class="product--variations-gallery-info-slider">
            <?php foreach (page()->children()->listed() as $variation): ?>
              <div class="product--variations-gallery-info-slider-item">
                <h3 class="product--variations-gallery-info-slider-item-title">
                  <?= $variation->title()->html() ?>
                </h3>
                <h4 class="product--variations-gallery-info-slider-item-short-title">
                  <?= $variation->shortTitle()->html() ?>
                </h4>

                <?php foreach ($variation->metaFields()->toStructure() as $meta): ?>
                  <div class="product--variations-gallery-info-slider-item-meta-item">
                    <p class="product--variations-gallery-info-slider-item-meta-item-field">
                      <?= $meta->field()->html(); ?>
                    </p>
                    <p class="product--variations-gallery-info-slider-item-meta-item-value">
                      <?= $meta->value()->kirbyText(); ?>
                    </p>
                  </div>
                <?php endforeach; ?>
              </div>
            <?php endforeach; ?>
          </div>

          <div class="product--variations-gallery-slider-container">
            <div class="product--variations-gallery-slider">
              <?php foreach (page()->children()->listed() as $variation): ?>
                <?php if ($variation->galleryImage()->isNotEmpty()): ?>
                  <div class="product--variations-gallery-slider-item">
                    <div class="product--variations-gallery-slider-item-image"
                         style="padding-top: <?= page()->variationImageRatio() ?>%; background-image: url('<?= $variation->galleryImage()->toFile()->resize(600)->url() ?>')"></div>
                    <div class="product--variations-gallery-slider-item-image hidden"
                         style="background-image: url('<?= $variation->secondGalleryImage()->toFile()->resize(600)->url() ?>')"></div>
                  </div>
                <?php endif; ?>
              <?php endforeach; ?>
            </div>

            <div class="product--variations-gallery-left"></div>
            <div class="product--variations-gallery-right"></div>
          </div>
        </div>
      </div>

      <div class="product--accordion-list">
        <div class="product--accordion">
          <div class="product--accordion-head">
            <h3 class="product--accordion-head-title">
              Request
            </h3>

            <?php snippet('icons/expand', ['class' => 'product--accordion-head-icon']) ?>

          </div>

          <div class="product--accordion-body">
            <div class="product--accordion-body-inner">
              <div class="product--accordion-column">
                <div class="product--accordion-request-left">
                  <?= page()->requestText()->kirbyText() ?>

                  <p class="product--accordion-request-success product-form-success">
                    THANK YOU, YOUR MESSAGE HAS BEEN SENT
                  </p>
                </div>
              </div>
              <div class="product--accordion-column">
                <form class="product--accordion-request-form product-form">
                  <input type="text" name="name" id="name" placeholder="Full name">
                  <input type="text" name="company" id="company" placeholder="Company name">
                  <input type="text" name="email" id="email" placeholder="Email address">
                  <input type="text" name="country" id="country" placeholder="Country">
                  <textarea name="message" rows="3" id="message" placeholder="Message"></textarea>

                  <p class="privacy-policy">
                    By submitting I agree to <a href="/privacy-policy" target="_blank">Privacy Policy</a>.
                  </p>

                  <button type="submit">
                    send
                    <?php snippet('icons/right') ?>
                  </button>

                  <div class="errors">
                    <p class="name-err">Please enter your name</p>
                    <p class="email-err">Please enter your email address</p>
                    <p class="email-invalid">Please enter a valid email address</p>
                    <p class="country-err">Please enter your country</p>
                    <p class="message-err">Please enter your message</p>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

        <?php if (page()->showSampleBoxAccordion()->toBool()): ?>
          <div class="product--accordion">
            <div class="product--accordion-head">
              <h3 class="product--accordion-head-title">
                Sample box
              </h3>

              <?php snippet('icons/expand', ['class' => 'product--accordion-head-icon']) ?>
            </div>

            <div class="product--accordion-body">
              <div class="product--accordion-body-inner">
                <div class="product--accordion-column">
                  <div class="product--accordion-sample-left-text">
                    <?= page()->sampleBoxLeftText()->kirbyText() ?>
                  </div>

                  <p class="product--accordion-sample-success product-form-success">
                    THANK YOU, YOUR ORDER HAS BEEN SUBMITTED. YOU WILL SHORTLY RECEIVE AN INVOICE AND FURTHER DETAILS BY
                    EMAIL.
                  </p>
                </div>
                <div class="product--accordion-column">
                  <form class="product--accordion-sample-form product-form">
                    <div class="client-type">
                      <label class="checkbox" for="sample-business">
                        <input type="radio" name="client-type" id="sample-business" value="business" checked="checked">
                        <span class="bubble"></span>
                        Business
                      </label>
                      <label class="checkbox" for="sample-physical">
                        <input type="radio" name="client-type" id="sample-physical" value="physical">
                        <span class="bubble"></span>
                        Private
                      </label>
                    </div>

                    <div class="business-fields open">
                      <input type="text" name="company" placeholder="Company name">
                      <input type="text" name="vat" placeholder="VAT">
                    </div>

                    <input type="text" name="name" placeholder="Full name">
                    <input type="text" name="email" placeholder="Email address">
                    <input type="text" name="phone" placeholder="Phone number (incl. int. code)">
                    <input type="text" name="address" placeholder="Address">
                    <input type="text" name="city" placeholder="City">
                    <input type="text" name="zip" placeholder="Postal code">
                    <input type="text" name="country" placeholder="Country">

                    <div class="business-fields open">
                      <label class="checkbox" for="sample-shipping">
                        <input type="checkbox" name="shipping" id="sample-shipping" checked="checked">
                        <span class="bubble"></span>
                        Shipping address is the same as billing
                      </label>

                      <div class="shipping-fields">
                        <input type="text" name="shipping-address" placeholder="Shipping address">
                        <input type="text" name="shipping-city" placeholder="City">
                        <input type="text" name="shipping-zip" placeholder="Postal code">
                        <input type="text" name="shipping-country" placeholder="Country">
                      </div>
                    </div>

                    <p class="privacy-policy">
                      By submitting I agree to <a href="/privacy-policy" target="_blank">Privacy Policy</a>.
                    </p>

                    <button type="submit">
                      send
                      <?php snippet('icons/right') ?>
                    </button>

                    <div class="errors">
                      <p class="company-err">Please enter your company name</p>
                      <p class="vat-err">Please enter your VAT code</p>
                      <p class="name-err">Please enter your name</p>
                      <p class="email-err">Please enter your email address</p>
                      <p class="email-invalid">Please enter a valid email address</p>
                      <p class="phone-err">Please enter your phone number</p>
                      <p class="address-err">Please enter your address</p>
                      <p class="city-err">Please enter your city</p>
                      <p class="zip-err">Please enter your postal code</p>
                      <p class="country-err">Please enter your country</p>
                      <p class="shipping-address-err">Please enter your shipping address</p>
                      <p class="shipping-city-err">Please enter your shipping city</p>
                      <p class="shipping-zip-err">Please enter your shipping postal code</p>
                      <p class="shipping-country-err">Please enter your shipping country</p>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        <?php endif; ?>

        <div class="product--accordion">
          <div class="product--accordion-head">
            <h3 class="product--accordion-head-title">
              Downloads
            </h3>

            <?php snippet('icons/expand', ['class' => 'product--accordion-head-icon']) ?>
          </div>

          <div class="product--accordion-body">
            <div class="product--accordion-body-inner product--accordion-body-inner---downloads">
              <?php foreach (page()->downloads()->toStructure() as $download): ?>
                <a href="<?= $download->file()->toFile()->url() ?>" download class="product--accordion-download-item">
                  <?php snippet('icons/download') ?>
                  <p>
                    <?= $download->title()->html() ?>
                  </p>
                </a>
              <?php endforeach; ?>
            </div>
          </div>
        </div>

        <?php if (page()->showInUseAccordion()->toBool()): ?>
          <div class="product--accordion">
            <div class="product--accordion-head">
              <h3 class="product--accordion-head-title">
                <?= page()->galleryLabel()->html() ?>
              </h3>

              <?php snippet('icons/expand', ['class' => 'product--accordion-head-icon']) ?>
            </div>

            <div class="product--accordion-body">
              <div class="product--accordion-body-inner">
                <div class="product--accordion-gallery-info">
                  <?php foreach (page()->gallery()->toStructure() as $i => $item): ?>
                    <div class="product--accordion-gallery-info-item gallery-info-<?= $i ?>"
                         style="<?= $i !== 0 ? 'display: none;' : '' ?>">
                      <?= $item->title()->kirbyText() ?>
                    </div>
                  <?php endforeach; ?>
                </div>

                <div class="gallery--container">
                  <div class="gallery impacts-height-product product--accordion-gallery-slider" data-dots="true">
                    <?php foreach (page()->gallery()->toStructure() as $item): ?>
                      <div class="gallery--item">
                        <div class="gallery--item-image"
                             style="padding-top: <?= page()->galleryRatio()->html() ?>%; background-image: url('<?= $item->image()->toFile()->resize(910)->url() ?>')"></div>
                      </div>
                    <?php endforeach; ?>
                  </div>

                  <?php if (sizeof(page()->gallery()->toStructure()) > 1): ?>
                    <div class="gallery--left"></div>
                    <div class="gallery--right"></div>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
        <?php endif; ?>

        <?php if (page()->showPressAccordion()->toBool()): ?>
          <div class="product--accordion">
            <div class="product--accordion-head">
              <h3 class="product--accordion-head-title">
                <?= page()->pressGalleryLabel()->html() ?>
              </h3>

              <?php snippet('icons/expand', ['class' => 'product--accordion-head-icon']) ?>
            </div>

            <div class="product--accordion-body">
              <div class="product--accordion-body-inner">
                <div class="product--accordion-gallery-info">
                  <?php foreach (page()->pressGallery()->toStructure() as $i => $item): ?>
                    <div class="product--accordion-gallery-info-item gallery-info-<?= $i ?>"
                         style="<?= $i !== 0 ? 'display: none;' : '' ?>">
                      <?= $item->title()->kirbyText() ?>
                    </div>
                  <?php endforeach; ?>
                </div>

                <div class="gallery--container">
                  <div class="gallery impacts-height-product product--accordion-gallery-slider" data-dots="true">
                    <?php foreach (page()->pressGallery()->toStructure() as $item): ?>
                      <div class="gallery--item">
                        <div class="gallery--item-image"
                             style="padding-top: <?= page()->pressGalleryRatio()->html() ?>%; background-image: url('<?= $item->image()->toFile()->resize(910)->url() ?>')"></div>
                      </div>
                    <?php endforeach; ?>
                  </div>

                  <?php if (sizeof(page()->pressGallery()->toStructure()) > 1): ?>
                    <div class="gallery--left"></div>
                    <div class="gallery--right"></div>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>

<?php snippet('footer') ?>