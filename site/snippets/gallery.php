<?php if (isset($items) && !empty($items)): ?>
  <div class="gallery--container <?= $class ?? '' ?> <?= isset($fill) && $fill ? 'gallery-fill' : '' ?>">
    <div
      class="gallery <?= $class ?? '' ?>" <?= isset($dots) && $dots ? 'data-dots="true"' : '' ?> <?= isset($adaptiveHeight) && $adaptiveHeight ? 'data-adaptive-height="true"' : '' ?>>
      <?php foreach ($items as $image): ?>
        <div class="gallery--item">
          <?php if (isset($fill) && $fill): ?>
            <?php if (isset($width)): ?>
              <div class="gallery--item-image gallery--item-image---fill desktop-only"
                   style="background-image: url('<?= $image->resize($width)->url(); ?>')"></div>
              <img src="<?= $image->resize($width)->url(); ?>" class="gallery--item-image---fill-image mobile-only">
            <?php else: ?>
              <div class="gallery--item-image gallery--item-image---fill desktop-only"
                   style="background-image: url('<?= $image->toFile()->url(); ?>')"></div>
              <img src="<?= $image->toFile()->url(); ?>" class="gallery--item-image---fill-image mobile-only">
            <?php endif; ?>
          <?php else: ?>
            <?php if (isset($adaptiveHeight) && $adaptiveHeight): ?>
              <?php if (isset($width)): ?>
                <img src="<?= $image->resize($width)->url(); ?>" class="gallery--item-image---adaptive">
              <?php else: ?>
                <img src="<?= $image->toFile()->url(); ?>" class="gallery--item-image---adaptive">
              <?php endif; ?>
            <?php else: ?>
              <?php if (isset($width)): ?>
                <div class="gallery--item-image"
                     style="padding-top: <?= $ratio ?? 9 / 16 * 100 ?>%; background-image: url('<?= $image->resize($width)->url(); ?>')"></div>
              <?php else: ?>
                <div class="gallery--item-image"
                     style="padding-top: <?= $ratio ?? 9 / 16 * 100 ?>%; background-image: url('<?= $image->toFile()->url(); ?>')"></div>
              <?php endif; ?>
            <?php endif; ?>
          <?php endif; ?>
        </div>
      <?php endforeach; ?>
    </div>

    <?php if (sizeof($items) > 1): ?>
      <div class="gallery--left"></div>
      <div class="gallery--right"></div>
    <?php endif; ?>
  </div>

<?php endif; ?>
