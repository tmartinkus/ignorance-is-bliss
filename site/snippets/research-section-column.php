<?php if (isset($column)): ?>

  <div class="research--column" style="width: <?= $column->width() ?>vw" data-width="<?= $column->width() ?>">
    <?php foreach($column->blocks()->toBlocks() as $block): ?>
      <?php snippet('research-blocks/' . $block->type(), ['block' => $block])?>
    <?php endforeach; ?>
  </div>

<?php endif; ?>
