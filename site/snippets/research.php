<?php if (isset($item)): ?>
  <div class="research" id="<?= str_replace('home/', '', $item->uri()) ?>"
       data-slug="<?= str_replace('home/', '', $item->uri()) ?>">

    <div class="research--head">
      <?php snippet('headings/research-' . $item->heading()) ?>

      <div class="research--mobile-button">
        Research
        <i></i>
      </div>
    </div>

    <div class="research--header">
      <div class="research--progress-bar"></div>

      <div class="container">
        <div class="research--progress">
          <?php $i = 0; ?>
          <?php foreach ($item->children()->listed() as $section): ?>
            <h3 class="research--progress-item <?= $i === 0 ? 'open' : '' ?>"
                data-index="<?= $i ?>"
                data-target="#research-<?= $section->parent()->slug() ?>-section-<?= $i ?>"
                id="progress-<?= $section->parent()->slug() ?>-item-<?= $i ?>">
              <?= $section->title()->html() ?>
            </h3>
            <?php $i++; ?>
          <?php endforeach; ?>
        </div>

        <?php snippet('burger') ?>
      </div>
    </div>

    <?php $width = 80; ?>
    <?php foreach ($item->children()->listed() as $section): ?>
      <?php $width += intval($section->width()->toString()); ?>
    <?php endforeach; ?>

    <div class="research--body">

      <div class="research--body-scroll-container">
        <?php $i = 0; ?>
        <?php foreach ($item->children()->listed() as $section): ?>
          <?php snippet('research-section', ['section' => $section, 'i' => $i]) ?>
          <?php $i++; ?>
        <?php endforeach; ?>

        <div class="research--section research--section---end">
          <div class="research--section-container">
            <?php snippet('headings/research-' . $end, ['noAnimation' => true]) ?>
          </div>
        </div>
      </div>

      <?php snippet('icons/left', ['class' => 'research--back']) ?>
    </div>
  </div>
<?php endif; ?>