</main>

<div class="cookies" id="cookies">
  <i class="cookies--close"></i>
  <div class="cookies--content">
    <?= site()->cookiesText()->kirbyText() ?>
  </div>
</div>

<footer class="footer">
  <div class="container">
    <div class="footer--top">
      <div class="footer--ig">
        <h3 class="footer--ig-heading">
          <?= site()->footerIgHeading() ?>
        </h3>
        <a href="<?= site()->footerIgUrl() ?>" target="_blank" class="footer--ig-link">
          <?= site()->footerIgUsername() ?>
        </a>
      </div>

      <?php snippet('icons/back-to-top', ['class' => 'footer--back-to-top', 'id' => 'back-to-top']) ?>
    </div>

    <div class="footer--bottom">
      <a href="/privacy-policy" target="_blank" class="footer--privacy-link">
        <?= site()->footerPrivacyPolicyText() ?>
      </a>
      <p class="footer--copyright">
        <?= site()->title() ?> | All rights reserved <?= date('Y') ?> ©
      </p>
    </div>
  </div>
</footer>

<?= js('/dist/bundle.js') ?>
</body>
</html>