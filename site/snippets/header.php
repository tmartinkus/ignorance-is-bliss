<!doctype html>
<html lang="en">
<head>
  <?php snippet('head/ga') ?>

  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <?php if (page()->uri() === 'home'): ?>
    <title><?= site()->title() ?></title>
    <meta name="og:title" content="<?= site()->title() ?>">
  <?php else:?>
    <title><?= page()->title() ?> | <?= site()->title() ?></title>
    <meta name="og:title" content="<?= page()->title() ?> | <?= site()->title() ?>">
  <?php endif; ?>

  <meta name="og:image" content="/src/images/og.jpg">
  <meta name="og:url" content="<?= page()->url() ?>">
  <meta name="og:type" content="website">
  <meta name="og:description" content="<?= $site->metaDescription()->html() ?>">
  <meta name="og:keywords" content="<?= $site->metaKeywords()->html() ?>">

  <link rel="apple-touch-icon" sizes="57x57" href="/src/images/favicons/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="/src/images/favicons/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/src/images/favicons/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/src/images/favicons/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/src/images/favicons/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="/src/images/favicons/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/src/images/favicons/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="/src/images/favicons/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="/src/images/favicons/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192" href="/src/images/favicons/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/src/images/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="/src/images/favicons/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/src/images/favicons/favicon-16x16.png">
  <link rel="manifest" href="/src/images/favicons/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="/src/images/favicons/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">

  <?= css('/dist/bundle.css') ?>
</head>

<?php
$bodyClass = page()->uid();

if (page()->parents()->count()) {
  $bodyClass = page()->parent()->uid();
}
?>

<body class="<?= $bodyClass ?>">

<?php if (page()->uid() !== 'home'): ?>
  <div class="header header---absolute">
    <div class="container">
      <h1 class="header--title"><?= page()->title() ?></h1>
    </div>
  </div>
<?php endif; ?>

<header class="header <?= page()->uid() === 'home' ? 'header---homepage' : '' ?>">
  <div class="container">
    <?php snippet('burger') ?>
  </div>
</header>

<main class="main">
