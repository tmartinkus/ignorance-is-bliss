<div class="news-list-item">
  <div class="news-list-item--head">
    <div class="news-list-item--head-container">
      <p class="news-list-item--date"><?= $item->date() ?></p>
      <h2 class="news-list-item--title"><?= $item->title() ?></h2>
    </div>

    <?php snippet('icons/expand', ['class' => 'news-list-item--expand']) ?>
  </div>

  <div class="news-list-item--body">
    <div class="news-list-item--body-column news-list-item--content">
      <?= $item->text()->kirbytext() ?>
    </div>

    <div class="news-list-item--body-column">
      <?php snippet('gallery', ['items' => $item->gallery()->toFiles(), 'dots' => true, 'class' => 'impacts-height-news', 'width' => 910, 'ratio' => $item->galleryRatio()->html()]); ?>
    </div>
  </div>
</div>