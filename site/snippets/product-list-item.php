<?php if (isset($product)): ?>
  <a href="<?= $product->url() ?>" class="product-list-item">
    <div class="product-list-item--image">
      <?= $product->listImage()->toFile()->resize(910) ?>
    </div>
    <h3 class="product-list-item--title">
      <?= $product->title()->html() ?>
    </h3>
    <p class="product-list-item--short-description">
      <?= $product->children()->count() ?> <?= $product->variationLabel()->html() ?>
    </p>
  </a>
<?php endif; ?>
