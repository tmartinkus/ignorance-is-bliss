<?php if (isset($block)): ?>
  <div class="research-block research-block---video">
    <video controls>
      <source src="<?= $block->video()->toFile()->url(); ?>" type="video/mp4">
      Your browser does not support the video tag.
    </video>
  </div>
<?php endif; ?>