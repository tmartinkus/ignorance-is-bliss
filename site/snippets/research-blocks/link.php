<?php if (isset($block)): ?>
  <a href="<?= $block->url()->html() ?>" target="<?= $block->target() ?>" class="research-block research-block---link">
    <?php snippet('icons/link') ?>
    <p><?= $block->title()->html() ?></p>
  </a>
<?php endif; ?>