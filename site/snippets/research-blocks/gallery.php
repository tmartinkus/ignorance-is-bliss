<?php if (isset($block)): ?>
  <div class="research-block research-block---gallery">
    <?php snippet('gallery', ['items' => $block->images()->toFiles(), 'width' => 1600, 'dots' => true, 'ratio' => $block->imageRatio()->html(), 'adaptiveHeight' => true]) ?>
  </div>
<?php endif; ?>