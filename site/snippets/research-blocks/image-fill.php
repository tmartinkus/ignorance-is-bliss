<?php if (isset($block)): ?>
  <div class="research-block research-block---image-fill">
    <div class="desktop-image" style="background-image: url('<?= $block->image()->toFile()->url() ?>');"></div>
    <img data-src="<?= $block->image()->toFile()->resize(1600)->url() ?>">
  </div>
<?php endif; ?>