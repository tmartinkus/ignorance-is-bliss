<?php if (isset($block)): ?>
  <div class="research-block research-block---image">
    <img data-src="<?= $block->image()->toFile()->resize(1600)->url() ?>" >
  </div>
<?php endif; ?>