<?php if (isset($block)): ?>
  <a href="<?= $block->file()->toFile()->url() ?>" download class="research-block research-block---download">
    <?php snippet('icons/download') ?>
    <p><?= $block->title()->html() ?></p>
  </a>
<?php endif; ?>