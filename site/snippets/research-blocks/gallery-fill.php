<?php if (isset($block)): ?>
  <div class="research-block research-block---gallery-fill">
    <?php snippet('gallery', ['items' => $block->images()->toFiles(), 'width' => 1600, 'dots' => true, 'fill' => true]) ?>
  </div>
<?php endif; ?>