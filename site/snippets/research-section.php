<?php if (isset($section)): ?>

  <div class="research--section"
       id="research-<?= $section->parent()->slug() ?>-section-<?= $i ?>"
       data-target="#progress-<?= $section->parent()->slug() ?>-item-<?= $i ?>">
    <div class="research--section-container">
      <?php foreach ($section->children()->listed() as $column): ?>
        <?php snippet('research-section-column', ['column' => $column]) ?>
      <?php endforeach; ?>
    </div>
  </div>

<?php endif; ?>
