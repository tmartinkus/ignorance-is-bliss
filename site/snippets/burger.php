<div class="burger">
  <div class="burger--list">
    <?php foreach ($pages->listed() as $item): ?>
      <?php if ($item->uid() !== 'privacy-policy'): ?>
        <a href="<?= $item->url() ?>"
           class="burger--list-item <?= $item->isOpen() ? 'open' : '' ?>"><?= $item->title()->html() ?></a>
      <?php endif; ?>
    <?php endforeach; ?>
  </div>
</div>

<div class="burger--button">
  <i></i><i></i><i></i>
</div>
