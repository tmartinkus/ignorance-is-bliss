<?php if (isset($variation) && $variation->listImage()->isNotEmpty()): ?>
  <div class="variation-grid-item"
       data-index="<?= $index ?>"
       style="width: <?= 100 / intval($desktopColumns->toString()) ?>%"
       data-desktop-width="<?= 100 / intval($desktopColumns->toString()) ?>%"
       data-laptop-width="<?= 100 / intval($laptopColumns->toString()) ?>%"
       data-mobile-width="<?= 100 / intval($mobileColumns->toString()) ?>%"
  >
    <img src="<?= $variation->listImage()->toFile()->resize(600)->url() ?>" alt="<?= $variation->title()->html() ?>"
         class="variation-grid-item--image">
    <h4 class="variation-grid-item--title">
      <?= $variation->title()->html() ?>
    </h4>
  </div>
<?php endif; ?>
